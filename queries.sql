create table dragons(
id serial not null, 
name varchar(256),
color varchar(256),
wingspan int,
primary key (id)
);

create table eggs(
id serial not null,
weight int,
diameter int,
dragon_id int,
primary key (id),
foreign key (dragon_id) references dragons (id)
);

create table ornaments(
egg_id int,
color varchar(256),
pattern varchar(256),
primary key(egg_id),
foreign key(egg_id) references eggs (id)
);

create table lands(
id serial not null,
name varchar(256),
primary key(id)
);

create table lands_dragons(
land_id int,
dragon_id int,
primary key (land_id, dragon_id),
foreign key (land_id) references lands (id),
foreign key (dragon_id) references dragons (id)
);

insert into dragons (name, color, wingspan) values
('Dygir', 'green', '200'),
('Bondril', 'red', '100'),
('Onosse', 'black', '250'),
('Chiri','yellow','50'),
('Lase', 'blue', '300');

insert into lands (name) values
('Froze'),
('Oswia'),
('Oscyae'),
('Oclurg');

insert into eggs (weight, diameter, dragon_id) select '300', '20', id from dragons where name='Dygir';
insert into eggs (weight, diameter, dragon_id) select '340', '30', id from dragons where name='Dygir';
insert into eggs (weight, diameter, dragon_id) select '200', '10', id from dragons where name='Bondril';
insert into eggs (weight, diameter, dragon_id) select '230', '20', id from dragons where name='Onosse';
insert into eggs (weight, diameter, dragon_id) select '300', '25', id from dragons where name='Onosse';
insert into eggs (weight, diameter, dragon_id) select '200', '15', id from dragons where name='Onosse';

insert into ornaments (color,pattern,egg_id) select 'pink', 'mesh', id from eggs where weight=300 and diameter=20;
insert into ornaments (color,pattern,egg_id) select 'black', 'striped', id from eggs where weight=340 and diameter=30;
insert into ornaments (color,pattern,egg_id) select 'yellow', 'dotted', id from eggs where weight=200 and diameter=10;
insert into ornaments (color,pattern,egg_id) select 'blue', 'dotted', id from eggs where weight=230 and diameter=20;
insert into ornaments (color,pattern,egg_id) select 'green', 'striped', id from eggs where weight=300 and diameter=25;
insert into ornaments (color,pattern,egg_id) select 'red', 'mesh', id from eggs where weight=200 and diameter=15;

insert into lands_dragons (land_id, dragon_id) values ((select id from lands where name='Froze'), (select id from dragons where name='Dygir'));
insert into lands_dragons (land_id, dragon_id) values ((select id from lands where name='Froze'), (select id from dragons where name='Bondril'));
insert into lands_dragons (land_id, dragon_id) values ((select id from lands where name='Oswia'), (select id from dragons where name='Dygir'));
insert into lands_dragons (land_id, dragon_id) values ((select id from lands where name='Oswia'), (select id from dragons where name='Onosse'));
insert into lands_dragons (land_id, dragon_id) values ((select id from lands where name='Froze'), (select id from dragons where name='Chiri'));

create view eggs_ornaments 
as select diameter, weight, color 
from eggs, ornaments 
where eggs.id = ornaments.egg_id;

create view dragons_lands (dragon_name, land_name) 
as select d.name, l.name 
from  dragons d, lands l, lands_dragons ld
where ld.land_id = l.id
and ld.dragon_id = d.id;

create view dragons_eggs
as select name, weight, diameter from dragons inner join eggs on
dragons.id = eggs.dragon_id;

create view eggless_dragons
as select name from dragons left join eggs on
dragons.id = eggs.dragon_id
where eggs.dragon_id is null;

select * from dragons;
select * from eggs;
select * from ornaments;
select * from lands;
select * from lands_dragons;
select * from eggs_ornaments;
select * from dragons_lands;
select * from dragons_eggs;
select * from eggless_dragons;

select name from dragons where wingspan between 200 and 400;

drop table dragons, eggs, ornaments, lands, lands_dragons cascade;


